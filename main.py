import os, time, io

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException

TRANSACTIONS_LEN = 10
ids = ['counter', 'shortDescription', 'reference', 'serviceFee', 'amount', 'ledgerBalance']
names = ['Date', 'Description', 'Reference', 'Service Fee', 'Amount', 'Balance']
all_info = []


def main():
    options = webdriver.ChromeOptions()

    driverDir = './chromedriver'
    driver = webdriver.Chrome(driverDir, chrome_options = options)
    driver.get('https://www.fnb.co.za/')
    
    username = driver.find_element_by_id('user')
    username.send_keys('tainebambrough145')

    password = driver.find_element_by_id('pass')
    password.send_keys('Work#123')
    
    time.sleep(2)

    loginButton = driver.find_element_by_id('OBSubmit')
    loginButton.click()
    
    time.sleep(7)

    driver.get('https://www.online.fnb.co.za/banking/Controller?nav=transactionhistory.navigator.TransactionHistoryRedirect&productCode=DDA&anrfn=-30433119&initial=initial&FARFN=98&pN=VHJhbnNhY3Rpb24gSGlzdG9yeSBSZWRpcmVjdA==')
    
    for i in range(1, TRANSACTIONS_LEN+1):
        store = {}
        str_1 = '//div[@id=\'tabelRow_' + str(i) + '\']'
        
        for j in range(len(ids)):
            str_2, _id = str_1, ids[j]

            if(_id == 'amount'):
                _id += '_' + str(i-1)
            
            str_2 += '//div[@id=\'' + _id + '\']'
            elem = driver.find_element_by_xpath(str_2)
            
            store[names[j]] = elem.text
    
        all_info.append(store)
    
    print all_info
    
    time.sleep(1000)
    driver.quit()


if(__name__ == "__main__"):
    main()

